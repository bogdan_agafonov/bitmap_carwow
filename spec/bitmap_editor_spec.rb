require 'spec_helper'
require 'bitmap_editor'

RSpec.describe BitmapEditor do
  let(:file) { 'examples/show.txt' }
  let(:expected_string) do
    "OOOOO\nOOKKK\nAWOOO\nOWOOO\nOWOOO\nOWOOO\n"
  end

  context 'integration' do
    describe "run on example file" do
      subject { BitmapEditor.run(file) }

      it "prints expected string" do
        expect { subject }.to output(expected_string).to_stdout
      end
    end
  end
end
