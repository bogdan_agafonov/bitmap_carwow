require 'spec_helper'
require 'bitmap_editor/command/clear_image'
require 'bitmap_editor/command/error'
require 'bitmap_editor/state'

RSpec.describe BitmapEditor::Command::ClearImage do
  let(:args) { [] }
  subject { described_class.new(*args) }

  let(:initial_screen) do
    [["W","X","O"],
     ["O","X","O"],
     ["X","O","T"]]
  end

  let(:expected_screen) { 3.times.map { 3.times.map { "O" }}}
  let(:initial_state) { BitmapEditor::State.new(initial_screen) }

  context "valid args" do
    describe "#apply" do
      it 'returns new state' do
        expect(subject.apply(initial_state)).to be_a(BitmapEditor::State)
      end

      it 'clears image' do
        expect(subject.apply(initial_state).screen).to eq(expected_screen)
      end
    end
  end
end
