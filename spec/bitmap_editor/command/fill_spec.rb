require 'spec_helper'
require 'bitmap_editor/command/fill'
require 'bitmap_editor/state'

RSpec.describe BitmapEditor::Command::Fill do
  let(:input_args) {  { colour: "G", x: 2, y: 3 }.values }
  let(:empty_screen) do
    BitmapEditor::State.new(
      [["O","O","O"],
       ["O","O","O"],
       ["O","O","O"]]
    )
  end
  let(:filled_screen) do
    [["G","G","G"],
     ["G","G","G"],
     ["G","G","G"]]
  end
  subject { described_class.new(*input_args) }

  describe "#apply" do
    context "empty screen" do
      it "fills the whole screen" do
        expect(subject.apply(empty_screen).screen).to eq(filled_screen)
      end
    end

    context "some shapes on the screen" do
      let(:input_args) {  { colour: "K", x: 1, y: 3 }.values }
      let(:initial_screen) do
        BitmapEditor::State.new(
          [["O","O","G"],
           ["O","O","O"],
           ["O","O","O"]]
        )
      end

      let(:filled_empty) do
        [["O","O","K"],
         ["O","O","O"],
         ["O","O","O"]]
      end

      context "fill pointing empty space" do
        it "fills empty space" do
          expect(subject.apply(initial_screen).screen).to eq(filled_empty)
        end
      end
    end

    context "one point" do
      let(:input_args) {  { colour: "K", x: 2, y: 2 }.values }
      let(:initial_screen) do
        BitmapEditor::State.new(
          [["O","O","G"],
           ["O","H","G"],
           ["O","O","O"]]
        )
      end

      let(:filled_empty) do
        [["O","O","G"],
         ["O","K","G"],
         ["O","O","O"]]
      end

      it "fills one point" do
        expect(subject.apply(initial_screen).screen).to eq(filled_empty)
      end
    end

    context "some shapes on the screen" do
      let(:input_args) {  { colour: "K", x: 1, y: 2 }.values }
      let(:initial_screen) do
        BitmapEditor::State.new(
          [["O","O","G"],
           ["O","O","G"],
           ["O","O","O"]]
        )
      end

      let(:filled_empty) do
        [["K","K","G"],
         ["K","K","G"],
         ["K","K","K"]]
      end

      context "fill pointing empty space" do
        it "fills empty space" do
          expect(subject.apply(initial_screen).screen).to eq(filled_empty)
        end
      end
    end
  end
end
