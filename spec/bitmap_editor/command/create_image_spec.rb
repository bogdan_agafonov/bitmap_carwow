require 'spec_helper'
require 'bitmap_editor/command/create_image'
require 'bitmap_editor/command/error'
require 'bitmap_editor/state'

RSpec.describe BitmapEditor::Command::CreateImage do
  let(:args) { ["100", "100"] }
  subject { described_class.new(*args) }

  context "valid args" do
    it 'returns new state' do
      expect(subject.apply(BitmapEditor::State.new([[]]))).to be_a(BitmapEditor::State)
    end
  end

  context 'image dimensions are too big' do
    let(:args) { ["1000", "1000"] }

    it 'raises an error' do
      expect { subject }.to raise_error(BitmapEditor::Command::Error)
    end
  end
end
