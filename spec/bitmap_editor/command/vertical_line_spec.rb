require 'spec_helper'
require 'bitmap_editor/command/vertical_line'
require 'bitmap_editor/state'
require 'bitmap_editor/command/error'

RSpec.describe BitmapEditor::Command::VerticalLine do
  let(:input_args) { { x: 2, y1: 1, y2: 2, colour: "X" }.values }
  subject { described_class.new(*input_args) }

  context 'valid args' do
    it 'is created' do
      expect(subject).to be_a(described_class)
    end
  end

  describe '#apply' do
    let(:empty_screen) do
      [["O","O","O"],
       ["O","O","O"],
       ["O","O","O"]]
    end
    let(:state) { BitmapEditor::State.new(empty_screen) }
    subject { described_class.new(*input_args).apply(state) }

    context 'applicable' do
      let(:expected_screen) do
        [["O","X","O"],
         ["O","X","O"],
         ["O","O","O"]]
      end

      it 'returns new state' do
        expect(subject).to be_a(BitmapEditor::State)
      end

      it 'draws a horizontal line' do
        expect(subject.screen).to eq(expected_screen)
      end
    end

    context 'not applicable' do
      let(:input_args) { [2, 2, 4, "X"] }

      it 'raises an error' do
        expect { subject }.to raise_error(BitmapEditor::Command::Error)
      end
    end
  end
end
