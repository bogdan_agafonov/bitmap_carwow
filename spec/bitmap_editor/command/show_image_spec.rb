require 'spec_helper'
require 'bitmap_editor/command/show_image'
require 'bitmap_editor/state'

RSpec.describe BitmapEditor::Command::ShowImage do
  let(:args) { [] }
  subject { described_class.new(*args) }
  let(:initial_screen) do
    [["W","X","O"],
     ["O","X","O"],
     ["X","O","T"]]
  end
  let(:initial_state) { BitmapEditor::State.new(initial_screen) }

  describe "#apply" do
    it 'returns state' do
      expect(subject.apply(initial_state)).to be_a(BitmapEditor::State)
    end

    it 'does nothing with the state' do
      expect(subject.apply(initial_state).screen).to eq(initial_screen)
    end
  end
end
