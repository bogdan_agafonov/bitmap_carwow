require 'spec_helper'
require 'bitmap_editor/operator'
require 'bitmap_editor/state'

RSpec.describe BitmapEditor::Operator do
  let(:fake_io) { StringIO.new }
  subject { described_class.new(fake_io) }
  let(:state) { instance_double(BitmapEditor::State, screen: [[]]) }

  describe "run_command" do
    context "create_image" do
      let(:command) { BitmapEditor::Command::CreateImage.new(5, 5) }

      subject { described_class.new(fake_io).run_command(state, command) }

      it 'returns new state' do
        expect(subject).to be_a(BitmapEditor::State)
      end

      it "returns new empty image" do
        expect(subject.screen).to eq(5.times.map { 5.times.map { "O" } })
      end
    end
  end
end
