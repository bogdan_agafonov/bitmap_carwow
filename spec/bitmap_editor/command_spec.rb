# command_spec.rb

require 'spec_helper'
require 'bitmap_editor/command'

RSpec.describe BitmapEditor::Command do
  let(:line_index) { 42 }
  subject { described_class.new(input) }

  describe 'constructor' do
    context 'valid input' do
      let(:input) { 'S' }

      it 'returns command instance if input is valid' do
        expect(subject).to be_a(BitmapEditor::Command::ShowImage)
      end
    end

    context 'invalid input' do
      context 'gibberrish - no command' do
        let(:input) { 'gibberish' }

        it 'raises an error' do
          expect { subject }.to raise_error(BitmapEditor::Command::Error)
        end
      end

      context 'looks like valid but it is not' do
        let(:input) { 'U 12 2 X' }

        it 'raises an error' do
          expect { subject }.to raise_error(BitmapEditor::Command::Error)
        end
      end

      context 'coordinate is too big' do
        let(:input) { 'L 12 251 X' }

        it 'raises an error' do
          expect { subject }.to raise_error(BitmapEditor::Command::Error)
        end
      end

      context 'coordinate is NaN' do
        let(:input) { 'L X 215 X' }

        it 'raises an error' do
          expect { subject }.to raise_error(BitmapEditor::Command::Error)
        end
      end

      context 'color is not valid' do
        let(:input) { 'L 12 214 .' }

        it 'raises an error' do
          expect { subject }.to raise_error(BitmapEditor::Command::Error)
        end
      end
    end
  end
end
