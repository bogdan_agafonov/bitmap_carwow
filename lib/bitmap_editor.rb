require 'logger'
$:.unshift(File.expand_path("../../lib", __FILE__))

require 'bitmap_editor/state'
require 'bitmap_editor/command'
require 'bitmap_editor/operator'

module BitmapEditor
  def self.run(file, logger_class = Logger)
    logger = logger_class.new(STDOUT)
    return puts "Please provide correct file" if file.nil? || !File.exists?(file)

    operator = Operator.new()
    state = State.new()

    File.open(file).each_with_index do |line, index|
      command = Command.new(line.chomp)
      state = operator.run_command(state, command)
    rescue Command::Error => e
      logger.warn("Error at line #{index + 1}: #{e.message}")
    end
  end
end
