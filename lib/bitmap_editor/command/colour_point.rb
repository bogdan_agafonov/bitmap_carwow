require 'bitmap_editor/command/base'
require 'bitmap_editor/command/error'
require 'bitmap_editor/command/input_validation_helpers'
require 'bitmap_editor/state'

module BitmapEditor
  module Command
    class ColourPoint < Base

      def initialize(x, y, colour)
        @x = x.to_i - 1
        @y = y.to_i - 1
        @colour = colour
        raise Error, "Colour point: invalid input" unless valid_input?
      end

      def valid_input?
        valid_colour?(@colour) && valid_coordinate?(@x) && valid_coordinate?(@y)
      end

      def apply(state)
        raise Error, "Colour point: not applicable" unless applicable?(state)
        new_state_array = state.screen.dup
        new_state_array[@y][@x] = @colour
        State.new(
          new_state_array
        )
      end

      def applicable?(state)
        @x < state.width && @y < state.height
      end
    end
  end
end
