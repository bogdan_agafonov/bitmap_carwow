require 'bitmap_editor/command/base'
require 'bitmap_editor/command/error'
require 'bitmap_editor/state'

module BitmapEditor
  module Command
    class Fill < Base
      def initialize(colour, x, y)
        @colour = colour
        @x = x.to_i - 1
        @y = y.to_i - 1
        raise Error, "Fill image: Invalid input" unless valid_input?
      end

      def apply(state)
        target_colour = find_original_colour_at_point(state.screen)
        State.new(flood_fill(@x, @y, target_colour, state.screen))
      end

      def flood_fill(x, y, target_colour, screen)
        return screen unless (screen[x] && screen[x][y])
        replacement_colour = @colour
        node_colour = screen[x][y]
        if target_colour == @colour
          screen
        elsif (node_colour != target_colour)
          screen
        else
          screen[x][y] = @colour
          screen = flood_fill(x+1, y, target_colour, screen)
          screen = flood_fill(x-1, y, target_colour, screen)
          screen = flood_fill(x, y+1, target_colour, screen)
          screen = flood_fill(x, y-1, target_colour, screen)
          screen
        end
      end

      private

      def find_original_colour_at_point(state)
        state[@x][@y]
      end

      def valid_input?
        valid_coordinate?(@y) &&
          valid_coordinate?(@x) &&
          valid_colour?(@colour)
      end
    end
  end
end
