require 'bitmap_editor/command/base'
require 'bitmap_editor/command/error'
require 'bitmap_editor/state'

module BitmapEditor
  module Command
    class VerticalLine < Base
      def initialize(x, y1, y2, colour)
        @x = x.to_i - 1
        @y1 = y1.to_i - 1
        @y2 = y2.to_i - 1
        @colour = colour
        raise Error, "Vertical line: invalid input" unless valid_input?
      end

      def apply(state)
        raise Error, "Vertical line: not applicable" unless applicable?(state)
        new_state_array = state.screen.dup
        new_state_array = draw_line(new_state_array)
        State.new(
          new_state_array
        )
      end

      private

      def valid_input?
        valid_coordinate?(@x) &&
        valid_coordinate?(@y1) &&
        valid_coordinate?(@y2) &&
        valid_colour?(@colour)
      end

      def applicable?(state)
        @x < state.width &&
          @y1 < state.height &&
          @y2 < state.height
      end

      def draw_line(screen)
        screen.map.with_index do |row, index|
          if in_range?(index)
            row[@x] = @colour
          end
          row
        end
      end

      def in_range?(index)
        Range.new(*[@y1, @y2].sort).include?(index)
      end
    end
  end
end
