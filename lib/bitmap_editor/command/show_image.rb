require 'bitmap_editor/command/base'

module BitmapEditor
  module Command
    class ShowImage < Base
      def needs_output?; true; end
    end
  end
end
