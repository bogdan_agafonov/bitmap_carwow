require 'bitmap_editor/state'

module BitmapEditor
  module Command
    module InputValidationHelpers
      VALID_COLOURS = ("A".."Z")

      def valid_colour?(colour)
        VALID_COLOURS.include?(colour)
      end

      def valid_coordinate?(value)
        value && value >= 0 &&  value < BitmapEditor::State::CANVAS_MAX_LENGTH
      end
    end
  end
end
