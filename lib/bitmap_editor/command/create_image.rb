require 'bitmap_editor/command/base'
require 'bitmap_editor/command/error'
require 'bitmap_editor/state'
require 'bitmap_editor/colours'

module BitmapEditor
  module Command
    class CreateImage < Base
      def initialize(width, height)
        @width = width.to_i
        @height = height.to_i
        raise Error, "Create image: invalid input" unless valid_input?
      end

      def valid_input?
        valid_coordinate?(@width) && valid_coordinate?(@height)
      end

      def apply(_state)
        State.new(
          @height.times.map { @width.times.map { BitmapEditor::Colours::WHITE } }
        )
      end
    end
  end
end
