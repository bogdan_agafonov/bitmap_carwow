require 'bitmap_editor/command/base'
require 'bitmap_editor/state'

module BitmapEditor
  module Command
    # clears an image
    class ClearImage < Base
      def apply(state)
        State.new(
          state.height.times.map { state.width.times.map { BitmapEditor::Colours::WHITE } }
        )
      end
    end
  end
end
