require 'bitmap_editor/command/input_validation_helpers'

module BitmapEditor
  module Command
    class Base
      include BitmapEditor::Command::InputValidationHelpers

      def needs_output?; false; end
      def apply(state)
        state
      end
    end
  end
end
