require 'bitmap_editor/command/base'
require 'bitmap_editor/command/error'
require 'bitmap_editor/state'

module BitmapEditor
  module Command
    class HorizontalLine < Base
      def initialize(x1, x2, y, colour)
        @x1 = x1.to_i - 1
        @x2 = x2.to_i - 1
        @y = y.to_i - 1
        @colour = colour
        raise Error, "Horizontal line: invalid input" unless valid_input?
      end

      def apply(state)
        raise Error, "Horizontal line: not applicable" unless applicable?(state)
        new_state_array = state.screen.dup
        new_state_array[@y] = draw_line(new_state_array[@y])
        State.new(
          new_state_array
        )
      end

      private

      def valid_input?
        valid_coordinate?(@y) &&
        valid_coordinate?(@x1) &&
        valid_coordinate?(@x2) &&
        valid_colour?(@colour)
      end

      def applicable?(state)
        @y < state.height &&
          @x1 <= state.width &&
          @x2 <= state.width
      end

      def draw_line(row)
        row.map.with_index do |elem, index|
          if in_range?(index)
            @colour
          else
            elem
          end
        end
      end

      def in_range?(index)
        Range.new(*[@x1, @x2].sort).include?(index)
      end
    end
  end
end
