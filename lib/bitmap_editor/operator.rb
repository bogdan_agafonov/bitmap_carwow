module BitmapEditor
  class Operator
    def initialize(output = $stdout)
      @output = output
    end

    def run_command(state, command)
      new_state = command.apply(state)
      if command.needs_output?
        @output.puts(new_state.to_s)
      end

      new_state
    end
  end
end
