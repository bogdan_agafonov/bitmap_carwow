require 'bitmap_editor/colours'
require 'bitmap_editor/state'
require 'bitmap_editor/command/error'
require 'bitmap_editor/command/create_image'
require 'bitmap_editor/command/clear_image'
require 'bitmap_editor/command/show_image'
require 'bitmap_editor/command/horizontal_line'
require 'bitmap_editor/command/vertical_line'
require 'bitmap_editor/command/colour_point'
require 'bitmap_editor/command/fill'

module BitmapEditor
  module Command
    VALID_COMMANDS = {
      "I" => CreateImage,
      "S" => ShowImage,
      "C" => ClearImage,
      "H" => HorizontalLine,
      "V" => VerticalLine,
      "L" => ColourPoint,
      "F" => Fill
    }.freeze

    class << self
      def new(line)
        parse(line.split(/\s+/))
      end

      def parse(tokens)
        VALID_COMMANDS.fetch(tokens.first).new(*tokens[1..-1])
      rescue KeyError => e
        raise Error, "Invalid command"
      end
    end
  end
end
