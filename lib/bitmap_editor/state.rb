module BitmapEditor
  class State
    CANVAS_MAX_LENGTH = 250

    attr_reader :screen, :height, :width

    def initialize(initial = [[]])
      @screen = initial
      @width = initial[0].size
      @height = initial.size
    end

    def to_s
      @screen.map { |row| row.join("") }.join("\n")
    end
  end
end
